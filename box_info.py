import time

class BoxInfo:

    def __init__(self):        
        self.product_weight = 0.0
        self.product_num = 0
        self.real_total_weight = 0.0
        self.bias = 0.0
        self.status = 'normal'
        self.predict_time = 0.5
        self.current_time = 0

        self.weight_buffer = []
        self.record_weight = False
        self.last_weight = 0.0 
        self.current_weight = 0.0
        self.changed_weight = 0.0

    def update(self, raw_weight):
        self.weight_buffer.append(raw_weight)
        if len(self.weight_buffer) > 10:
            self.weight_buffer = self.weight_buffer[1:]
            self.current_weight = self.weight_buffer[5]

    def check_buffer(self):
        if max(self.weight_buffer) - min(self.weight_buffer) > 7 and self.record_weight == False:
            self.record_weight = True
            self.current_time = time.time()
            self.last_weight = self.current_weight

    def consume_check(self):
        if self.record_weight and float(time.time()) - self.current_time > self.predict_time:
            self.changed_weight = self.last_weight - self.current_weight
            self.record_weight = False