import unittest
import weight_gettor
import config_settor
import weight_sensor_engine

class TestStringMethods(unittest.TestCase):

    def test_weight_gettor(self):
        weightgettor = weight_gettor.WeightGettor()
        weightgettor.start()
        self.assertEqual(len(weightgettor.get_value()), 2)

    def test_config_settor(self):
        config_path = 'test_sensor_info.json'
        configsettor = config_settor.ConfigSettor(config_path)
        dic = {'product_weight': [1, 2], 'product_num': [1, 2], 'real_total_weight': [1, 2]}
        self.assertEqual(dic, configsettor.get_sensor_info())

    def test_split(self):
        config_path = 'test_sensor_info.json'
        weightsensorengine = weight_sensor_engine.WeightSensorEngine(config_path)

if __name__ == '__main__':
    unittest.main()