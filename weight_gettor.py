# -*- coding: utf-8 -*-
import serial
import threading
import time
import numpy as np


class WeightGettor():

    def __init__(self):
        self.arduino_value = serial.Serial("com5",9600)
        self.sensor_num = 2    
        self.start() 
        self.weight_value_list = []
        self.check_value = True 

    def start(self):
	    # 把程式放進子執行緒，daemon=True 表示該執行緒會隨著主執行緒關閉而關閉。
        threading.Thread(target=self.query_value, daemon=True, args=()).start()

    def query_value(self):
        time.sleep(5)
        while True:  
            try:
                self.weight_value_list = self.arduino_value.readline().decode().strip('\r\n').split(',')
                # self.weight_value_list = [900,12872]
            except:
                print('Weight Sensor Error')

    def get_value(self):
        while self.weight_value_list == []:
            time.sleep(0.1)

        while self.check_value:
            try:
                weight_list = [float(i) for i in self.weight_value_list]
                self.check_value = False
            except:
                print(self.weight_value_list)
        weight_list = [float(i) for i in self.weight_value_list]
        weight_list = weight_list[:self.sensor_num]#arduino有時候會給超過sensor數量的長度
        return weight_list

if __name__ == '__main__':
    weightgettor = WeightGettor()
    weightgettor.start()
    print(weightgettor.get_value()[0])