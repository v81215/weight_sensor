from tkinter import scrolledtext #文字滾動條
from tkinter import ttk #下拉選單
import tkinter as tk
import threading
import time
import weight_sensor_engine
import config_settor
import time
import csv_writer

class UI:

    def __init__(self):
        self.init_time = time.time()
        self.write_time = time.time()
        self.open_time = 0
        self.csvwriter = csv_writer.CsvWriter()

        self.door_status = False
        self.test_case_1_status = False
        self.test_case_2_status = False

        self.weightsensorengine = weight_sensor_engine.WeightSensorEngine('sensor_info.json')
        self.window = tk.Tk()
        self.window.title('Weight Sensor')
        self.window.geometry('1000x800') 

        self.var = tk.StringVar()
        self.text = scrolledtext.ScrolledText(self.window,font=('微軟雅黑',14),fg='blue')
        self.text.pack()

        self.l = tk.Label(self.window, textvariable = self.var, bg='green', fg='white', font=('Arial', 14), width=30, height=2)       
        self.l.pack()

        value = self.set_combobox_value()
        self.comboExample = ttk.Combobox(self.window, 
                            values = value)
        self.comboExample.pack()
        self.comboExample.current(1)  

        self.set_weight_button = tk.Button(self.window, text = 'set weight' ,font=('Arial', 12), width=10, height=1, command = self.set_weight)
        self.set_weight_button.pack()
        
        self.door_open_button = tk.Button(self.window, text = 'open' ,font=('Arial', 12), width=10, height=1, command = self.open_door)
        self.door_open_button.pack()
        self.door_close_button = tk.Button(self.window, text = 'close' ,font=('Arial', 12), width=10, height=1, command = self.close_door)
        self.door_close_button.pack()
        self.test_case_1_button = tk.Button(self.window, text = 'test case 1' ,font=('Arial', 12), width=10, height=1, command = self.start_test_case_1)
        self.test_case_1_button.pack()
        self.test_case_2_button = tk.Button(self.window, text = 'test case 2' ,font=('Arial', 12), width=10, height=1, command = self.start_test_case_2)
        self.test_case_2_button.pack()
        threading.Thread(target=self.update_weight_value, daemon=True, args=()).start()
        self.window.mainloop()
        
    def set_combobox_value(self):
        value = []
        for index, box_info in enumerate(self.weightsensorengine.box_info_list):
            value.append(str(index))
        return value

    def update_weight_value(self):
        
        while 1: 
            if self.test_case_1_status:
                self.test_case_1()
            if self.test_case_2_status:
                self.test_case_2()

            self.weightsensorengine.poll()
            # self.text.insert('end','box total weight : ' + str(self.weightsensorengine.revise_value())+'\n')
            for index, box_info in enumerate(self.weightsensorengine.box_info_list): 
                self.text.insert('end','box ' + str(index) + 'product num:' + str(box_info.product_num) + ' take weight: ' + str(box_info.real_total_weight - self.weightsensorengine.revise_value()[index]) +'\n')
            self.text.see("end")
            time.sleep(0.1)

    def open_door(self):
        self.var.set('open door')
        self.weightsensorengine.enable()

    def close_door(self):    
        self.var.set('close door')
        self.weightsensorengine.disable()

    def set_weight(self):
        self.var.set(str(self.weightsensorengine.revise_value()[int(self.comboExample.get())]))
        self.weightsensorengine.set_product_weight(int(self.comboExample.get()))
        print(int(self.comboExample.get()))

    def start_test_case_1(self):
        if self.test_case_1_status == False:
            self.test_case_1_status = True
            self.var.set('start test case 1')
        else:
            self.test_case_1_status = False
            self.var.set('stop test case 1')

    def start_test_case_2(self):
        if self.test_case_2_status == False:
            self.test_case_2_status = True
            self.open_door()
            self.var.set('start test case 2')
        else:
            self.test_case_2_status = False
            self.close_door()
            self.var.set('stop test case 2')

    def test_case_1(self):
        product_weight = self.weightsensorengine.revise_value()
        if time.time() - self.init_time > 5:
                self.open_door()
                self.door_status = True
                self.init_time = time.time()
                self.open_time = time.time()
        if time.time() - self.open_time > 3:
            self.close_door()
            self.door_status = False
        if self.door_status:
            if time.time() - self.write_time >= 1:
                data = [str(time.strftime("%d%H%M%S", time.localtime())),str(product_weight[0]), str(product_weight[1]), str(self.weightsensorengine.box_info_list[0].bias), str(self.weightsensorengine.box_info_list[1].bias)]
                self.csvwriter.write_down('test_case_1.csv',data)
                self.write_time = time.time()
    
    def test_case_2(self):
        product_weight = self.weightsensorengine.revise_value()
        if time.time() - self.write_time >= 1:
            data = [str(time.strftime("%d%H%M%S", time.localtime())),str(product_weight[0]), str(product_weight[1]), str(self.weightsensorengine.box_info_list[0].bias), str(self.weightsensorengine.box_info_list[1].bias)]
            self.csvwriter.write_down('test_case_2.csv',data)
            self.write_time = time.time()

if __name__ == '__main__':
    ui = UI()