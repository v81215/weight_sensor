import json
import os

class ConfigSettor:

    def __init__(self, config_path):
        self.config_path = config_path 
        if os.path.isfile(self.config_path):
            with open(config_path,'r') as read_file:
                self.sensor_info = json.load(read_file)
                
    def get_sensor_info(self):
        return self.sensor_info

    def product_weight_set(self, product_weight_list):
        self.sensor_info['product_weight'] = product_weight_list
       
    def product_num_set(self, product_num_list):
        self.sensor_info['product_num'] = product_num_list

    def real_total_weight_set(self, total_weight_list):
        self.sensor_info['real_total_weight'] = total_weight_list

    def save_info(self):
        data = json.dumps(self.sensor_info)
        with open(self.config_path, 'w') as fp:
            fp.write(data)
        
        

if __name__ == '__main__':
    config = ConfigSettor('sensor_info.json')
    config.save_info()
