import weight_gettor
import box_info
import config_settor


class WeightSensorEngine:

    def __init__(self,config_path):
        self.box_info_list = []
        self.weightgettor = weight_gettor.WeightGettor()
        self.configsettor = config_settor.ConfigSettor(config_path)
        self.sensor_info_list = self.configsettor.get_sensor_info()
        self.create_box_info()
        
    def create_box_info(self):        
        
        for index in range(len(self.sensor_info_list['product_weight'])):
            _box_info = box_info.BoxInfo()            
            _box_info.product_weight = self.sensor_info_list['product_weight'][index]
            _box_info.product_num = self.sensor_info_list['product_num'][index]
            _box_info.real_total_weight = self.sensor_info_list['real_total_weight'][index]
            self.box_info_list.append(_box_info)

    def enable(self):
        product_weight = self.weightgettor.get_value()
        for index, box_info in enumerate(self.box_info_list):
            box_info.real_total_weight = self.weightgettor.get_value()[index]
            # box_info.bias = float(product_weight[index]) - box_info.product_weight * box_info.product_num
            # print('bias:',box_info.bias)

    def disable(self):
        product_weight = []
        product_num = []
        real_total_weight = []
        revise_weight = self.revise_value()
         
        for index, box_info in enumerate(self.box_info_list):
            box_info.real_total_weight = revise_weight[index]
            product_weight.append(box_info.product_weight)
            product_num.append(box_info.product_num)
            real_total_weight.append(box_info.real_total_weight)
        self.configsettor.product_weight_set(product_weight)
        self.configsettor.product_num_set(product_num)
        self.configsettor.real_total_weight_set(real_total_weight)
        self.configsettor.save_info() 

    def revise_value(self):
        finetune_value_list = []
        weight = self.weightgettor.get_value()
        for index, box_info in enumerate(self.box_info_list):            
            finetune_value_list.append(round(float(weight[index]) - box_info.bias, 2))
        return finetune_value_list


    def poll(self):
        revise_weight = self.revise_value()
        for index, box_info in enumerate(self.box_info_list):
            if box_info.product_weight > 0:
                num = int((box_info.real_total_weight - revise_weight[index]) / box_info.product_weight + 0.5)
                box_info.product_num = num
                
                #補貨模式不用檢查數量
                # if int(num+0.5) > int(box_info.product_num+0.5):
                #     box_info.status = 'abnormal'
                # elif abs(int(num) - num) >= 0.2:
                #     box_info.status = 'abnormal'
                # else:
                #     box_info.status = 'normal'
                # box_info.product_num = int(num+0.5)
            # box_info.real_total_weight = revise_weight[index]
        
        return self.box_info_list 


    def set_product_weight(self, index):
        self.box_info_list[index].product_weight = round(self.revise_value()[index], 2)
        print(self.box_info_list[index].product_weight)
        

    
   
    
    # def reset_value(self):
    #     for index in range(len(self.bias_list)):
    #         self.bias_list[index] = float(self.weight_value_list[index]) - self.product_weight_list[index] * self.product_num_list[index]

if __name__ == '__main__':
    weightsensorengine = WeightSensorEngine('sensor_info.json')
    weightsensorengine.enable()
    weightsensorengine.disable()
    # print(weightsensorengine.box_info_list[0].product_num)